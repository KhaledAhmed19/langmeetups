import {createStore,compose,applyMiddleware} from 'redux';
import{persistStore,autoRehydrate} from 'redux-persist';
import reducer from '../reducer';
import thunk from 'redux-thunk';
import {AsyncStorage} from 'react-native';

var defaultState = {}

exports.configureStore = (initialState=defaultState) => {
    var store =  createStore (reducer,
        initialState, 
        compose(applyMiddleware(thunk)) );

   
    return store;
}