import axios from 'axios'

import {SIGNIN_URL, SIGNUP_URL} from '../api';


exports.loginUser = (email,password) => {
    return  function (dispatch) {
        return axios.post(SIGNIN_URL, {email,password}).then
        ((response) => {
            var {user_id,token} = response.data;
            dispatch (authUser(user_id));
        }).catch ((error) => {});
    }

            
}

exports.signupUser = (email,password) => {
    return function (dispatch)  {
        return axios.post(SIGNUP_URL, {email,password}).then
        ((response) => {
            var {user_id,token} = response.data;
            dispatch (authUser(user_id));
        }).catch ((error) => {});
    }
    
            
}



exports.authUser = (user_id)=>{
    return {
        type: 'AUTH_USER',
        user_id
    }
}

exports.unauthUser = {
    type: 'UNAUTH_USER',
    
}