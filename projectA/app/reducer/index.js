import uuid from 'uuid'

import {reducer as formReducer} from 'redux-form'
import {combineReducers} from 'redux'
import authReducer from './authReducer'



var defaultState = {};
 module.exports = combineReducers({
     form: formReducer,
    auth: authReducer
 });

