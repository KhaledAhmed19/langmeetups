import React from 'react';
import { StyleSheet, Text,ScrollView,  View, TouchableOpacity,StatusBar,TextInput} from 'react-native';
import {connect} from 'react-redux';

import {changeText} from '../actions';
import {addTodo,deleteTodo} from '../actions';
import {inc} from '../actions';
import Login from './Login'
import Main2 from './Main2'



  class Main extends React.Component {
  render() {

    if (this.props.user_id){
      return (
        <Main2 />
      );
    } else {
      return (
        <Login />

      );
    }
  }
}

var mapStateToProps = (state) => {
  return {
    user_id: state.auth.user_id
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'stretch',
    justifyContent: 'flex-start',
  },
  topBar: {
    padding: 16,
    paddingTop: 28,
    paddingBottom: 8,
    flexDirection: 'row',
    backgroundColor: 'green',
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    color: 'white',
    fontSize: 20,
   
  },
  inputContainer: {
    padding: 8,
    padding: 0,
    backgroundColor: 'green'
   
  },
  input: {
    height: 26,
    padding: 4 ,
    paddingLeft: 8,
    borderRadius: 8,
    backgroundColor: 'white'
   
  },
  todoContainer: {
    borderTopWidth:1,
    borderBottomWidth:1,
    marginBottom:-1,
    padding:  20,
    flexDirection:'row',
    borderColor: '#ccf',
    justifyContent:'space-between',
    alignItems:'center'
   
  },

});





module.exports = 
   connect(
  mapStateToProps
)(Main)


    

    







