import React from 'react';
import { StyleSheet, Text,ScrollView,  View, TouchableOpacity,StatusBar,TextInput} from 'react-native';
import {connect} from 'react-redux';

import {changeText} from '../actions';
import {addTodo,deleteTodo} from '../actions';
import {inc} from '../actions';
import Login from './Login'


class Main2 extends React.Component {
    render() {
        return (
  
    <View style = {styles.container}>
    <Text>
        Welcome to Main
        </Text>
        </View>
        );
    }
  }

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: 'white',
      alignItems: 'center',
      justifyContent: 'center',
    }
})

module.exports = Main2;